package ru.tsc.chertkova.tm.controller;

import ru.tsc.chertkova.tm.api.controller.ICommandController;
import ru.tsc.chertkova.tm.api.service.ICommandService;
import ru.tsc.chertkova.tm.model.Command;
import ru.tsc.chertkova.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            System.out.println(command.toString());
        }
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.15.0");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Yuliya Chertkova");
        System.out.println("ychertkova@t1-consulting.com");
    }

    @Override
    public void showWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    @Override
    public void showError(final String param) {
        System.out.printf("Error! This argument '%s' not supported...\n", param);
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            String name = command.getName();
            if (name != null && !name.isEmpty())
                System.out.println(command.getName());
        }
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getCommands();
        for (final Command command : commands) {
            String argument = command.getArgument();
            if (argument != null && !argument.isEmpty())
                System.out.println(command.getArgument());
        }
    }

}
